module git.cv-library.co.uk/twagle/moda

go 1.16

require (
	bitbucket.org/twagle/modb v0.0.3 // indirect
	bitbucket.org/twagle/modb/v2 v2.1.0
)
