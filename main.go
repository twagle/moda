package main

// https://bitbucket.org/twagle/modb/src/master/
//import "git.cv-library.co.uk/twagle/modb" // this is a private package (served via our private go mod proxy)
//import "bitbucket.org/twagle/modb" // just use bitbucket directly! .. why not? (this path is for v0, v1 only)
import "bitbucket.org/twagle/modb/v2" // just use bitbucket directly! .. why not? (note changed path for v2..n)

func main() {
	modb.Hello()
	modb.World()
	modb.Version()
}
