#!/bin/bash
# Run Athens with disk storage and ssh to bitbucket's git repos.

ATHENS_HOME="$HOME/.athens"
mkdir -p $ATHENS_HOME
mkdir -p $ATHENS_HOME/storage

# .gitconfig contains the http to ssh rewrite rule (http is used by go get to interrogate bitbucket)
# git clone git@bitbucket.org:twagle/modb.git
# https://bitbucket.org/twagle/modb/src/master/
cat << EOF > $ATHENS_HOME/.gitconfig
[url "git@bitbucket.org:"]
    insteadOf = https://bitbucket.org/scm
EOF

# WARNING: this is sharing your private SSH keys with Athens!
docker run --rm -d \
    -v "$ATHENS_HOME/storage:/var/lib/athens" \
    -v "$ATHENS_HOME/.gitconfig:/root/.gitconfig" \
    -v "$HOME/.ssh:/root/.ssh" \
    -e ATHENS_DOWNLOAD_MODE="sync" \
    -e ATHENS_DISK_STORAGE_ROOT=/var/lib/athens -e ATHENS_STORAGE_TYPE=disk --name athens-proxy -p 3000:3000 gomods/athens:latest


