#!/bin/bash
# docker run -d -p '3000:3000' gomods/athens:latest

export GOPROXY="http://localhost:3000"  # the Athens proxy server

# don't use the public checksum database to authenticate modules with paths starting with "_" or "git.cv-library.co.uk"
# should OT require this?
#export GONOSUMDB="_,git.cv-library.co.uk"

